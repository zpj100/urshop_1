﻿namespace Urs.Core.Plugins
{
    /// <summary>
    /// 插件接口
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// 插件配置页面网址
        /// </summary>
        string GetConfigurationPageUrl();
        /// <summary>
        /// 插件描述
        /// </summary>
        PluginDescriptor PluginDescriptor { get; set; }
        /// <summary>
        /// 安装插件
        /// </summary>
        void Install();

        /// <summary>
        /// 卸载插件
        /// </summary>
        void Uninstall();
    }
}
