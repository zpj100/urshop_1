﻿
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class MediaSettings : ISettings
    {
        public int AvatarPictureSize { get; set; }
        public int GoodsThumbPictureSize { get; set; }
        public int GoodsDetailsPictureSize { get; set; }
        public int GoodsThumbPictureSizeOnGoodsDetailsPage { get; set; }
        public int CategoryThumbPictureSize { get; set; }
        public int BrandThumbPictureSize { get; set; }
        public int CartThumbPictureSize { get; set; }
        public int AutoCompleteSearchThumbPictureSize { get; set; }
        public bool DefaultPictureZoomEnabled { get; set; }

        public int MaximumImageSize { get; set; }

        /// <summary>
        /// Geta or sets a default quality used for image generation
        /// </summary>
        public int DefaultImageQuality { get; set; }
    }
}