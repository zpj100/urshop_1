﻿
using Urs.Core.Configuration;
using Urs.Data.Domain.Users;

namespace Urs.Data.Domain.Configuration
{
    public class UserSettings : ISettings
    {
        /// <summary>
        /// 默认密码格式
        /// </summary>
        public PasswordFormat DefaultPasswordFormat { get; set; }

        /// <summary>
        /// Hash格式 (SHA1, MD5)
        /// </summary>
        public string HashedPasswordFormat { get; set; }

        /// <summary>
        ///密码最低长度
        /// </summary>
        public int PasswordMinLength { get; set; }
        /// <summary>
        /// Account JWT
        /// </summary>
        public string JwtIssuer { get; set; }
        public string JwtAudience { get; set; }
        public string JwtSecurityKey { get; set; }
        public int JwtExpires { get; set; }
        /// <summary>
        ///头像最大限制大小
        /// </summary>
        public int AvatarMaximumSizeBytes { get; set; }
        /// <summary>
        /// 限制细调IP地址
        /// </summary>
        public bool StoreIpAddresses { get; set; }
        /// <summary>
        /// 用户格式
        /// </summary>
        public UserNameFormat UserNameFormat { get; set; }
        /// <summary>
        /// 在线客服
        /// </summary>
        public int OnlineUserMinutes { get; set; }
    }
}