﻿using Urs.Core;
using System.Collections.Generic;

namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 参数值
    /// </summary>
    public partial class GoodsParameterOption : BaseEntity
    {
        private ICollection<GoodsParameterMapping> _goodsParameters;

        public virtual int GoodsParameterId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        public virtual bool Deleted { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int DisplayOrder { get; set; }
        /// <summary>
        /// 映射
        /// </summary>
        public virtual GoodsParameter Mapping { get; set; }
        public virtual ICollection<GoodsParameterMapping> GoodsParameters
        {
            get { return _goodsParameters ?? (_goodsParameters = new List<GoodsParameterMapping>()); }
            protected set { _goodsParameters = value; }
        }
    }
}
