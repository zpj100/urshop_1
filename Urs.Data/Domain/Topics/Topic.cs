using Urs.Core;
namespace Urs.Data.Domain.Topics
{
    /// <summary>
    /// Represents a topic
    /// </summary>
    public partial class Topic : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public virtual string SystemName { get; set; }
        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public virtual string Short { get; set; }

        /// <summary>
        /// Gets or sets the title
        /// </summary>
        public virtual string Title { get; set; }

        /// <summary>
        /// Gets or sets the body
        /// </summary>
        public virtual string Body { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether class is published
        /// </summary>
        public virtual bool Published { get; set; }
    }
}
