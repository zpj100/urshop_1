using System;
using Urs.Core;
using Urs.Data.Domain.Orders;

namespace Urs.Data.Domain.Users
{
    /// <summary>
    /// Represents a reward point history entry
    /// </summary>
    public partial class PointsHistory : BaseEntity
    {
        /// <summary>
        /// Gets or sets the user identifier
        /// </summary>
        public virtual int UserId { get; set; }

        /// <summary>
        /// Gets or sets the points redeemed/added
        /// </summary>
        public virtual int Points { get; set; }

        /// <summary>
        /// Gets or sets the points balance
        /// </summary>
        public virtual int PointsBalance { get; set; }

        /// <summary>
        /// Gets or sets the used amount
        /// </summary>
        public virtual decimal UsedAmount { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public virtual string Message { get; set; }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public virtual DateTime CreateTime { get; set; }

        /// <summary>
        /// Gets or sets the order for which points were redeemed as a payment
        /// </summary>
        public virtual Order UsedWithOrder { get; set; }

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public virtual User User { get; set; }
    }
}
