
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Security;

namespace Urs.Data.Mapping.Security
{
    public partial class PermissionRecordMap : UrsEntityTypeConfiguration<PermissionRecord>
    {
        public override void Configure(EntityTypeBuilder<PermissionRecord> builder)
        {
            builder.ToTable(nameof(PermissionRecord));
            builder.HasKey(pr => pr.Id);
            builder.Property(pr => pr.Name).IsRequired();
            builder.Property(pr => pr.SystemName).IsRequired().HasMaxLength(255);
            builder.Property(pr => pr.Group).IsRequired().HasMaxLength(255);

            base.Configure(builder);
        }
    }
}