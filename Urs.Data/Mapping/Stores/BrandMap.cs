
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class BrandMap : UrsEntityTypeConfiguration<Brand>
    {
        public override void Configure(EntityTypeBuilder<Brand> builder)
        {
            builder.ToTable(nameof(Brand));
            builder.HasKey(m => m.Id);
            builder.Property(m => m.Name).IsRequired().HasMaxLength(400);
            builder.Property(m => m.Description);
            builder.Property(m => m.MetaKeywords).HasMaxLength(400);
            builder.Property(m => m.MetaDescription);
            builder.Property(m => m.MetaTitle).HasMaxLength(400);
            builder.Property(m => m.PriceRanges).HasMaxLength(400);
            base.Configure(builder);
        }
    }
}