﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class ShopMap : UrsEntityTypeConfiguration<Shop>
    {
        public override void Configure(EntityTypeBuilder<Shop> builder)
        {
            builder.ToTable(nameof(Shop));
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Code);
            builder.Property(p => p.Name);

            base.Configure(builder);

        }
    }
}
