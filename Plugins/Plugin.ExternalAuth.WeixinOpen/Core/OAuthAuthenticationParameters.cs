using System;
using Urs.Services.Authentication.External;

namespace ExternalAuth.WeixinOpen.Core
{
    [Serializable]
    public class OAuthAuthParameters : OpenAuthParameters
    {
        private readonly string _providerSystemName;

        public OAuthAuthParameters(string providerSystemName)
        {
            this._providerSystemName = providerSystemName;
            this.UserClaim = new UserClaims();
        }

        public override string ProviderSystemName
        {
            get { return _providerSystemName; }
        }
    }
}