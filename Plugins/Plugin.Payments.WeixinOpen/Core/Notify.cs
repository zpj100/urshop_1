﻿using Microsoft.AspNetCore.Http;
using System;
using System.Text;

namespace tenpayApp
{
    /// <summary>
    /// 回调处理基类
    /// 主要负责接收微信支付后台发送过来的数据，对数据进行签名验证
    /// 子类在此类基础上进行派生并重写自己的回调处理过程
    /// </summary>
    public class Notify
    {
        protected IHttpContextAccessor _httpContextAccessor;
        public Notify(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 接收从微信支付后台发送过来的数据并验证签名
        /// </summary>
        /// <returns>微信支付后台返回的数据</returns>
        public WxPayData GetNotifyData(string appKey)
        {
            var response = _httpContextAccessor.HttpContext.Response;
            //接收从微信后台POST过来的数据
            System.IO.Stream s = _httpContextAccessor.HttpContext.Request.Body;
            int count = 0;
            byte[] buffer = new byte[1024];
            StringBuilder builder = new StringBuilder();
            while ((count = s.Read(buffer, 0, 1024)) > 0)
            {
                builder.Append(Encoding.UTF8.GetString(buffer, 0, count));
            }
            s.Flush();
            s.Close();
            s.Dispose();
            //转换数据格式并验证签名
            WxPayData data = new WxPayData(appKey);
            try
            {
                data.FromXml(builder.ToString());
            }
            catch (Exception ex)
            {
                //若签名错误，则立即返回结果给微信支付后台
                WxPayData res = new WxPayData(appKey);
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", ex.Message);
                response.WriteAsync(res.ToXml());
            }
            return data;
        }

        //派生类需要重写这个方法，进行不同的回调处理
        public virtual string ProcessNotify(string appId, string appKey, string partner)
        {
            return string.Empty;
        }
    }
}