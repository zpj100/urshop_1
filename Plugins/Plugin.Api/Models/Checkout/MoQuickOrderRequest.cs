﻿using Plugin.Api.Models.Common;
using Urs.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Plugin.Api.Models.UserAddress;

namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 快捷订单数据
    /// </summary>
    public class MoQuickOrderRequest
    {
        public MoQuickOrderRequest()
        {
            GoodsValues = new List<GoodsValue>();
        }
        /// <summary>
        /// 支付方法：微信小程序： Payments.WeixinOpen  微信端微信支付：Payments.WeixinWap
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// 余额付款
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// 地址Id
        /// </summary>
        public int AddressId { get; set; }
        /// <summary>
        /// 收货地址
        /// </summary>
        public MoAddress Address { get; set; }
        /// <summary>
        /// 订单总价
        /// </summary>
        public decimal OrderTotal { get; set; }
        /// <summary>
        /// 订单备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 商品与数量
        /// </summary>
        public IList<GoodsValue> GoodsValues { get; set; }

        public partial class GoodsValue
        {
            /// <summary>
            /// 商品Sku
            /// </summary>
            public string Sku { get; set; }
            /// <summary>
            /// 商品数量
            /// </summary>
            public int Qty { get; set; }
        }
    }
}