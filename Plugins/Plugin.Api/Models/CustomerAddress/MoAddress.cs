﻿using System.Collections.Generic;

namespace Plugin.Api.Models.UserAddress
{
    /// <summary>
    /// 地址
    /// </summary>
    public class MoAddress
    {
        public MoAddress()
        {
            Fields = new List<MoCustomField>();
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 省份名称
        /// </summary>
        public string ProvinceName { get; set; }
        /// <summary>
        /// 城市名称
        /// </summary>
        public string CityName { get; set; }
        /// <summary>
        /// 县区名称
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// 地址详情
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 是否默认地址
        /// </summary>
        public bool Default { get; set; }

        /// <summary>
        /// 自定义字段
        /// </summary>
        public IList<MoCustomField> Fields { get; set; }

        public partial class MoCustomField
        {
            /// <summary>
            /// Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 默认值
            /// </summary>
            public string DefaultValue { get; set; }
        }

    }
}