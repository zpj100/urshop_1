﻿namespace Urs.Plugin.Misc.Api.Models.Agents
{
    /// <summary>
    /// 代理信息
    /// </summary>
    public partial class MyAgentInfo
    {
        /// <summary>
        /// 代理邀请码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 代理昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 是否审核通过，成为代理
        /// </summary>
        public bool Approved { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        public string Realname { get; set; }
        /// <summary>
        /// 代理手机号
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 代理头像
        /// </summary>
        public string ImgUrl { get; set; }
        /// <summary>
        /// 代理二维码 二维码暂时
        /// </summary>
        public string QRCode { get; set; }
        /// <summary>
        /// 总金额
        /// </summary>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 可用金额
        /// </summary>
        public decimal AvailAmount { get; set; }
        /// <summary>
        /// 冻结金额
        /// </summary>
        public decimal FreezingAmount { get; set; }
        /// <summary>
        /// 我的客户数量
        /// </summary>
        public int MyUserCount { get; set; }
        /// <summary>
        /// 我的订单数量
        /// </summary>
        public int MyOrderCount { get; set; }
        /// <summary>
        /// 我的分红金额
        /// </summary>
        public decimal MyBonusAmount { get; set; }
        /// <summary>
        /// 分红数量
        /// </summary>
        public int MyBonusCount { get; set; }
    }
}