﻿using System;
using System.Collections.Generic;
using Urs.Framework;
using Urs.Framework.Mvc;
using Plugin.Api.Models.Common;
using Urs.Framework.Models;

namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 用户积分
    /// </summary>
    public partial class MoPoints 
    {
        public MoPoints()
        {
            Items = new List<RewardPointsHistoryModel>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 积分明细
        /// </summary>
        public IList<RewardPointsHistoryModel> Items { get; set; }
        /// <summary>
        /// 可用积分
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// 积分价值
        /// </summary>
        public string AmountVale { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }

        #region Nested classes
        /// <summary>
        /// 积分记录
        /// </summary>
        public partial class RewardPointsHistoryModel : BaseEntityModel
        {
            /// <summary>
            /// 积分
            /// </summary>
            public decimal Points { get; set; }
            /// <summary>
            /// 积分余额
            /// </summary>
            public decimal Balance { get; set; }
            /// <summary>
            /// 说明
            /// </summary>
            public string Msg { get; set; }
            /// <summary>
            /// 日期
            /// </summary>
            public DateTime Time { get; set; }
        }

        #endregion
    }
}