﻿using FluentValidation;
using Urs.Data.Domain.Configuration;
using Plugin.Api.Models.Account;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Plugin.Api.Validators.User
{
    public class MoRegisterValidator : BaseUrsValidator<MoRegister>
    {
        public MoRegisterValidator(ILocalizationService localizationService, UserSettings userSettings)
        {
            RuleFor(x => x.Phone).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Phone.Required"));

        }
    }
}