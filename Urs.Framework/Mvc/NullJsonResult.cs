﻿using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Urs.Framework.Mvc
{
    public class NullJsonResult : JsonResult
    {
        public NullJsonResult() : base(null)
        {
        }
    }
}
