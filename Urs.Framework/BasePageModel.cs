
namespace Urs.Framework
{
    /// <summary>
    /// ������ҳ������
    /// </summary>
    public abstract class BasePageModel
    {
        public int PageNumber { get; set; }

        public int PageSize { get; set; }
        public int PageIndex
        {
            get
            {
                if (PageNumber > 0)
                    return PageNumber - 1;
                else
                    return default(int);
            }
        }
    }

    public class PagingFiltering : BasePageModel { }
}