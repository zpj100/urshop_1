using System;
using Urs.Data.Domain.Configuration;

namespace Urs.Services.Shipping
{
    public static class ShippingExtentions
    {
        public static bool IsShippingRateComputationMethodActive(this IShippingRateMethod srcm,
            ShippingSettings shippingSettings)
        {
            if (srcm == null)
                throw new ArgumentNullException("srcm");

            if (shippingSettings == null)
                throw new ArgumentNullException("shippingSettings");

            if (shippingSettings.ActiveShippingRateComputationMethodSystemNames == null)
                return false;
            foreach (string activeMethodSystemName in shippingSettings.ActiveShippingRateComputationMethodSystemNames)
                if (srcm.PluginDescriptor.SystemName.Equals(activeMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            return false;
        }
        
        public static void GetDimensions(this GetShippingOptionRequest request,
            out decimal width, out decimal length, out decimal height, bool useCubeRootMethod = true)
        {
            if (useCubeRootMethod)
            {
                decimal totalVolume = 0;
                decimal maxGoodsWidth = 0;
                decimal maxGoodsLength = 0;
                decimal maxGoodsHeight = 0;
                foreach (var shoppingCartItem in request.Items)
                {
                    var goods = shoppingCartItem.Goods;
                    if (goods != null)
                    {
                        totalVolume += shoppingCartItem.Quantity * goods.Height * goods.Width * goods.Length;

                        if (goods.Width > maxGoodsWidth)
                            maxGoodsWidth = goods.Width;
                        if (goods.Length > maxGoodsLength)
                            maxGoodsLength = goods.Length;
                        if (goods.Height > maxGoodsHeight)
                            maxGoodsHeight = goods.Height;
                    }
                }
                decimal dimension = Convert.ToDecimal(Math.Pow(Convert.ToDouble(totalVolume), (double)(1.0 / 3.0)));
                length = width = height = dimension;

                if (width < maxGoodsWidth)
                    width = maxGoodsWidth;
                if (length < maxGoodsLength)
                    length = maxGoodsLength;
                if (height < maxGoodsHeight)
                    height = maxGoodsHeight;
            }
            else
            {
                width = length = height = decimal.Zero;
                foreach (var shoppingCartItem in request.Items)
                {
                    var goods = shoppingCartItem.Goods;
                    if (goods != null)
                    {
                        width += goods.Width * shoppingCartItem.Quantity;
                        length += goods.Length * shoppingCartItem.Quantity;
                        height += goods.Height * shoppingCartItem.Quantity;
                    }
                }
            }
        }

        public static decimal GetTotalWidth(this GetShippingOptionRequest request)
        {
            decimal length, width, height = 0;
            GetDimensions(request, out width, out length, out height);
            return width;
        }

        public static decimal GetTotalLength(this GetShippingOptionRequest request)
        {
            decimal length, width, height = 0;
            GetDimensions(request, out width, out length, out height);
            return length;
        }

        public static decimal GetTotalHeight(this GetShippingOptionRequest request)
        {
            decimal length, width, height = 0;
            GetDimensions(request, out width, out length, out height);
            return height;
        }

    }
}
