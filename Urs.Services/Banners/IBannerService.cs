using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Banners;

namespace Urs.Services.Banners
{
    public partial interface IBannerService
    {
        #region Banner

        void DeleteBannerZone(Banner entity);

        IPagedList<Banner> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);

        Banner GetById(int id);

        Banner GetByZone(string name);

        void InsertBannerZone(Banner entity);

        void UpdateBannerZone(Banner entity);

        #endregion

        #region BannerItem

        void InsertItem(BannerItem entity);
        BannerItem GetBannerItemById(int id);
        IList<BannerItem> GetItemsById(int id);
        IList<BannerItem> GetItemsByZone(string zone);
        void UpdateItem(BannerItem entity);
        void DeleteItem(BannerItem entity);

        #endregion
    }
}
