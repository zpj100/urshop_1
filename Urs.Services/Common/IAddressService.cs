

using System.Collections.Generic;
using Urs.Data.Domain.Common;

namespace Urs.Services.Common
{
    public partial interface IAddressService
    {
        #region shipping address
        void DeleteAddress(Address address);

        Address GetAddressById(int addressId);
        IList<Address> GetAddresses(int userId);
        void InsertAddress(Address address);

        void UpdateAddress(Address address);

        #endregion

    }
}