﻿using System.Collections.Generic;
using Urs.Data.Domain.Orders;

namespace Urs.Services.Orders
{
    public partial class PlaceOrderResult
    {
        public IList<string> Errors { get; set; }

        public PlaceOrderResult() 
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get { return (this.Errors.Count == 0); }
        }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }

        
        public Order PlacedOrder { get; set; }
    }
}
