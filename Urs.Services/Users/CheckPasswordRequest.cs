﻿using Urs.Data.Domain.Users;

namespace Urs.Services.Users
{
    public class CheckPasswordRequest
    {
        public User User { get; set; }
        public string ConfirmPassword { get; set; }

        public CheckPasswordRequest(User user, string confirmPassword)
        {
            this.User = user;
            this.ConfirmPassword = confirmPassword;
        }
    }
}
