﻿namespace Urs.Services.Agents
{
    public partial class AgentSummary
    {
        public int NumberOfSum { get; set; }

        public int NumberOfMonth { get; set; }

        public int NumberOfPass { get; set; }

        public int NumberOfNoPass { get; set; }
    }
}
