
using System.Collections.Generic;
using Urs.Data.Domain.Users;

namespace Urs.Services.Authentication.External
{
    public partial interface IOpenAuthenticationService
    {
        IList<IExternalAuthenticationMethod> LoadActiveExternalAuthenticationMethods();

        IExternalAuthenticationMethod LoadExternalAuthenticationMethodBySystemName(string systemName);

        IList<IExternalAuthenticationMethod> LoadAllExternalAuthenticationMethods();

        bool AccountExists(OpenAuthParameters parameters);

        void AssociateExternalAccountWithUser(User user, OpenAuthParameters parameters);

        User GetUser(OpenAuthParameters parameters);

        IList<ExternalAuth> GetExternalIdentifiersFor(User user);

        void RemoveAssociation(OpenAuthParameters parameters);

        void DeleteExternalAuthenticationRecord(ExternalAuth record);

        void UpdateExternalAuthenticationRecord(ExternalAuth record);
        void InsertExternalAuthenticationRecord(ExternalAuth record);

        ExternalAuth GetExternalAuthentication(OpenAuthParameters parameters);
        ExternalAuth GetExternalAuthenticationRecordByOpenId(string openid);
    }
}