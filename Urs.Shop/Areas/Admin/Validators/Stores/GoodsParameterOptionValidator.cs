﻿using FluentValidation;
using Urs.Admin.Models.Stores;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Urs.Admin.Validators.Stores
{
    public class GoodsParameterOptionValidator : BaseUrsValidator<GoodsParameterOptionModel>
    {
        public GoodsParameterOptionValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotNull().WithMessage(localizationService.GetResource("Admin.Store.GoodsParameters.Options.Fields.Name.Required"));
        }
    }
}