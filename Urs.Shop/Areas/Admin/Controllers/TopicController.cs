﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Urs.Admin.Models.Topics;
using Urs.Data.Domain.Topics;
using Urs.Data.Domain.Configuration;
using Urs.Services.Localization;
using Urs.Services.Security;
using Urs.Services.Topics;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Framework.Kendoui;
using Urs.Admin.Models.Common;

namespace Urs.Admin.Controllers
{
    [AdminAuthorize]
    public partial class TopicController : BaseAdminController
    {
        #region Fields

        private readonly ITopicService _topicService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly AdminAreaSettings _adminAreaSettings;

        #endregion Fields

        #region Constructors

        public TopicController(ITopicService topicService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            AdminAreaSettings adminAreaSettings)
        {
            this._topicService = topicService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._adminAreaSettings = adminAreaSettings;
        }

        #endregion

        #region List

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTopics))
                return HttpUnauthorized();

            return View();
        }

        [HttpPost]
        public IActionResult ListJson(PageRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTopics))
                return HttpUnauthorized();

            var topics = _topicService.GetAllTopics();
            var result = new ResponseResult 
            {
                data = topics.Select(x =>
                {
                    var topicModel = x.ToModel<TopicModel>();
                    topicModel.Url = Url.RouteUrl("Topic", new { SystemName = x.SystemName }, "http");
                    return topicModel;
                }),

                count = topics.Count
            };
            return Json(result);
        }

        public IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTopics))
                return HttpUnauthorized();

            var topic = _topicService.GetTopicById(id);
            if (topic == null)
            {
                //No topic found with the specified id
                var models = new TopicModel();
                return View(models);
            }

            var model = topic.ToModel<TopicModel>();
            model.Url = Url.RouteUrl("Topic", new { SystemName = topic.SystemName }, "http");

            return View(model);

        }

        [HttpPost]
        public IActionResult Edit(TopicModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTopics))
                return HttpUnauthorized();
            var topic = _topicService.GetTopicById(model.Id);
            if (topic == null)
            {
                var topics = model.ToEntity<Topic>();
                _topicService.InsertTopic(topics);
            }
            else
            {
                model.Url = Url.RouteUrl("Topic", new { SystemName = topic.SystemName }, "http");
                if (ModelState.IsValid)
                {
                    topic = model.ToEntity(topic);
                    _topicService.UpdateTopic(topic);
                }
            }
            return Json(new { success = 1 });
        }

        [HttpPost]
        public IActionResult Published(CheckboxModel checkbox)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageCatalog))
                return HttpUnauthorized();

            var goods = _topicService.GetTopicById(checkbox.Id);
            if (goods == null)
                return Json(new { error = 1 });

            if (checkbox.Checked)
                goods.Published = true;
            else
                goods.Published = false;
            _topicService.UpdateTopic(goods);

            return Json(new { success = 1 });
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageTopics))
                return HttpUnauthorized();

            var topic = _topicService.GetTopicById(id);
            if (topic == null)
                return Json(new { error = 1 });

            _topicService.DeleteTopic(topic);

            return Json(new { success = 1 });
        }

        #endregion

    }
}
