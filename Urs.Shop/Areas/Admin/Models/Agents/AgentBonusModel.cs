﻿using System;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Agents
{
    public partial class AgentBonusModel : BaseEntityModel
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 订单金额
        /// </summary>
        public string OrderTotal { get; set; }
        /// <summary>
        /// 分销金额
        /// </summary>
        public string Fee { get; set; }
        /// <summary>
        /// 是否已提现
        /// </summary>
        public bool Cash { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }
    }
}