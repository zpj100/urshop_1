﻿using Microsoft.AspNetCore.Routing;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.ExternalAuthentication
{
    public partial class AuthenticationMethodModel : BaseEntityModel, IPluginModel
    {
        [UrsDisplayName("Admin.Configuration.ExternalAuthenticationMethods.Fields.FriendlyName")]

        public string FriendlyName { get; set; }

        [UrsDisplayName("Admin.Configuration.ExternalAuthenticationMethods.Fields.SystemName")]

        public string SystemName { get; set; }

        [UrsDisplayName("Admin.Configuration.ExternalAuthenticationMethods.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        public string ConfigurationUrl { get; set; }
        public string LogoUrl { get; set; }

        [UrsDisplayName("Admin.Configuration.ExternalAuthenticationMethods.Fields.IsActive")]
        public bool IsActive { get; set; }
        public string ConfigurationActionName { get; set; }
        public string ConfigurationControllerName { get; set; }
        public RouteValueDictionary ConfigurationRouteValues { get; set; }
    }
}