﻿namespace Urs.Admin.Models.Common
{
    public partial class SelectItemModel
    {
        public string name { get; set; }
        public string value { get; set; }
        public string selected { get; set; }
        public string disabled { get; set; }
    }
}