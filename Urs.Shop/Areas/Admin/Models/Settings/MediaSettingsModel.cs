﻿using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class MediaSettingsModel : BaseModel, ISettingsModel
    {
        [UrsDisplayName("Admin.Configuration.Settings.Media.PicturesStoredIntoDatabase")]
        public bool PicturesStoredIntoDatabase { get; set; }

        [UrsDisplayName("Admin.Configuration.Settings.Media.AvatarPictureSize")]
        public int AvatarPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.GoodsThumbPictureSize")]
        public int GoodsThumbPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.GoodsDetailsPictureSize")]
        public int GoodsDetailsPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.GoodsThumbPictureSizeOnGoodsDetailsPage")]
        public int GoodsThumbPictureSizeOnGoodsDetailsPage { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.GoodsVariantPictureSize")]
        public int GoodsVariantPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.CategoryThumbPictureSize")]
        public int CategoryThumbPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.BrandThumbPictureSize")]
        public int BrandThumbPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.CartThumbPictureSize")]
        public int CartThumbPictureSize { get; set; }
        [UrsDisplayName("Admin.Configuration.Settings.Media.MiniCartThumbPictureSize")]
        public int MiniCartThumbPictureSize { get; set; }
        
        [UrsDisplayName("Admin.Configuration.Settings.Media.MaximumImageSize")]
        public int MaximumImageSize { get; set; }
    }
}