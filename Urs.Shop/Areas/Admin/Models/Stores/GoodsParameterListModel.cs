﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    public partial class GoodsParameterListModel : BaseEntityModel
    {
        public GoodsParameterListModel()
        {
            AvailableGroups = new List<SelectListItem>();
        }
        /// <summary>
        /// 根据名称查找
        /// </summary>
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.GoodsParameters.Fields.ParameterGroupId")]
        public int ParameterGroupId { get; set; }

        public IList<SelectListItem> AvailableGroups { get; set; }
    }
}