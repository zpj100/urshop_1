using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class OrderIncompleteReportLineModel : BaseModel
    {
        [UrsDisplayName("Admin.SalesReport.Incomplete.Item")]
        public string Item { get; set; }

        [UrsDisplayName("Admin.SalesReport.Incomplete.Total")]
        public string Total { get; set; }

        [UrsDisplayName("Admin.SalesReport.Incomplete.Count")]
        public int Count { get; set; }
        [UrsDisplayName("Admin.SalesReport.Incomplete.View")]
        public string ViewLink { get; set; }
    }
}
