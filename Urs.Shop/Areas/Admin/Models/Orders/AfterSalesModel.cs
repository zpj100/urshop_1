﻿using FluentValidation.Attributes;
using System;
using System.Collections.Specialized;
using Urs.Admin.Validators.Orders;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Orders
{
    [Validator(typeof(AfterSalesValidator))]
    public partial class AfterSalesModel : BaseEntityModel
    {
        public AfterSalesModel()
        {
            ThumbnailImages = new NameValueCollection();
        }

        [UrsDisplayName("Admin.AfterSales.Fields.ID")]
        public override int Id { get; set; }

        [UrsDisplayName("Admin.AfterSales.Fields.Order")]
        public int OrderId { get; set; }

        [UrsDisplayName("Admin.AfterSales.Fields.User")]
        public int UserId { get; set; }

        public string UserName { get; set; }

        public int GoodsId { get; set; }
        [UrsDisplayName("Admin.AfterSales.Fields.Goods")]
        public string GoodsName { get; set; }

        [UrsDisplayName("Admin.AfterSales.Fields.Quantity")]
        public int Quantity { get; set; }

        
        [UrsDisplayName("Admin.AfterSales.Fields.ReasonForReturn")]
        public string ReasonForReturn { get; set; }

        
        [UrsDisplayName("Admin.AfterSales.Fields.RequestedAction")]
        public string RequestedAction { get; set; }

        
        [UrsDisplayName("Admin.AfterSales.Fields.UserComments")]
        public string UserComments { get; set; }
        
        [UrsDisplayName("Admin.AfterSales.Fields.Images")]
        public NameValueCollection ThumbnailImages { get; set; }

        
        [UrsDisplayName("Admin.AfterSales.Fields.StaffNotes")]
        public string StaffNotes { get; set; }

        [UrsDisplayName("Admin.AfterSales.Fields.Status")]
        public int AfterSalesStatusId { get; set; }
        [UrsDisplayName("Admin.AfterSales.Fields.Status")]
        public string AfterSalesStatusStr { get; set; }

        [UrsDisplayName("Admin.AfterSales.Fields.CreateTime")]
        public DateTime CreateTime { get; set; }
    }
}